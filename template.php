<?php


/**
 * @file
 */

/**
* Override or insert PHPTemplate variables into the templates.
*/
function aquanaut_preprocess_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  // Adjust ie.css path if color module is working its magic
  $color_paths = variable_get('color_aquanaut_stylesheets', array());
  if (!empty($color_paths)) {
    $vars['color_ie_path'] = base_path() . $color_paths[3];
    $vars['color_ie6_path'] = base_path() . $color_paths[1];
    $vars['color_ie7_path'] = base_path() . $color_paths[2];
  }

  // Make the site mission available to each page
  $vars['mission'] = variable_get('site_mission', '');
 
  // Custom settings
  $right_width = theme_get_setting('right_width'); 
  if (is_numeric($right_width)) {
    $vars['right_outer_width'] = $right_width;
    $vars['right_inner_width'] = $right_width - 22;
    $vars['left_column_width'] = 950 - $right_width;
  }
  else {
    $vars['right_outer_width'] = 320;
    $vars['right_inner_width'] = 298;
    $vars['left_column_width'] = 610;
  }
}

/**
 * Implementation of theme_menu_local_tasks().
 */
function aquanaut_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<div class=\"localtasks\"><ul class=\"tabs primary clearfix\">\n". $primary ."</ul></div>\n";
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= "<div class=\"localtasks\"><ul class=\"tabs secondary clearfix\">\n". $secondary ."</ul></div>\n";
  }

  return $output;
}
