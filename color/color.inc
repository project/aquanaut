<?php

$info = array(

  // Pre-defined color schemes.
  'schemes' => array(
    '#e0e0e0,#cc3f00,#4ec700,#efff14,#333333' => t('Aquanaut (Default)'),
    '#0072b9,#027ac6,#2385c2,#5ab5ee,#494949' => t('Blue Lagoon'),
    '#464849,#2f416f,#2a2b2d,#5d6779,#494949' => t('Ash'),
    '#55c0e2,#000000,#085360,#007e94,#696969' => t('Aquamarine'),
    '#d5b048,#6c420e,#331900,#971702,#494949' => t('Belgian Chocolate'),
    '#3f3f3f,#336699,#6598cb,#6598cb,#000000' => t('Bluemarine'),
    '#d0cb9a,#917803,#efde01,#e6fb2d,#494949' => t('Citrus Blast'),
    '#0f005c,#434f8c,#4d91ff,#1a1575,#000000' => t('Cold Day'),
    '#c9c497,#0c7a00,#03961e,#7be000,#494949' => t('Greenbeam'),
    '#ffe23d,#a9290a,#fc6d1d,#a30f42,#494949' => t('Mediterrano'),
    '#788597,#3f728d,#a9adbc,#d4d4d4,#707070' => t('Mercury'),
    '#5b5fa9,#5b5faa,#0a2352,#9fa8d5,#494949' => t('Nocturnal'),
    '#7db323,#6a9915,#b5d52a,#7db323,#191a19' => t('Olivia'),
    '#12020b,#1b1a13,#f391c6,#f41063,#898080' => t('Pink Plastic'),
    '#b7a0ba,#c70000,#a1443a,#f21107,#515d52' => t('Shiny Tomato'),
    '#18583d,#1b5f42,#34775a,#52bf90,#2d2d2d' => t('Teal Top'),
  ),

  // Images to copy over.
  'copy' => array(
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'style.css',
    'ie6.css',
    'ie7.css',
    'ie.css',
  ),

  // Coordinates of gradient (x, y, width, height).
  'gradient' => array(663, 10, 395, 105),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 1100, 500),
    'top' => array(989, 135, 15, 50),
    'bottom' => array(660, 9, 399, 106),
    'link' => array(29, 33, 100, 58),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/body.png'                      => array(0, 0, 10, 10),
    'images/content-bg.png'                => array(150, 75, 10, 10),
    'images/header-bg.png'                 => array(110, 0, 60, 400),

    // Header block top row
    'images/head-block-top-left.png'       => array(650, 0, 11, 20),
    'images/head-block-top-center.png'     => array(663, 0, 396, 20),
    'images/head-block-top-right.png'      => array(1059, 0, 11, 20),

    'images/head-block-content-gradient-bg.png'    => array(663, 12, 396, 96),

    'images/head-block-content-outer.png'  => array(650, 107, 11, 1),
    'images/head-block-content-bg.png'     => array(663, 107, 396, 1),
    'images/head-block-content-inner.png'  => array(1059, 107, 11, 1),

    'images/head-block-bottom-left.png'    => array(650, 109, 11, 20),
    'images/head-block-bottom-center.png'  => array(663, 109, 396, 20),
    'images/head-block-bottom-right.png'   => array(1059, 109, 11, 20),

    'images/li_collapsed.png'                   => array(92, 42, 15, 15),
    'images/li_leaf.png'              => array(92, 57, 15, 15),
    'images/li_expanded.png'               => array(92, 72, 15, 15),
    'logo.png'                             => array(29, 33, 48, 58),
    'screenshot.png'                       => array(0, 37, 400, 240),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
