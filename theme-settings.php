<?php

/**
 * @file
 * Settings for the Aquanaut theme.
 */

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function aquanaut_settings($saved_settings) {
  $defaults = array(
    'right_width' => 320,
  );

  $settings = array_merge($defaults, $saved_settings);

  $form['right_width'] = array(
    '#type' => 'textfield',
    '#title' => 'Width of right column in pixels',
    '#description' => 'How wide would you like the right column to be? e.g. 350',
    '#default_value' => $settings['right_width'],
  );

  return $form;
}
