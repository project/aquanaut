<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <style type = 'text/css'>
      #header-wrapper,
      #left-column {
        width: <?php print $left_column_width ?>px;
      }
      #right-wrapper {
        width: <?php print $right_outer_width ?>px;
      }
      #head-block-top-center,
      #head-block-bottom-center {
        width: <?php print $right_inner_width ?>px;
      }
    </style>
    <?php print $scripts ?>
    <!--[if IE]>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print (!empty($color_ie_path)) ? $color_ie_path : base_path() . path_to_theme() . '/ie.css' ?>" />
    <![endif]-->
    <!--[if IE 7]>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print (!empty($color_ie7_path)) ? $color_ie7_path : base_path() . path_to_theme() . '/ie7.css' ?>" />
    <![endif]-->
    <!--[if IE 6]>
      <link type="text/css" rel="stylesheet" media="all" href="<?php print (!empty($color_ie6_path)) ? $color_ie6_path : base_path() . path_to_theme() . '/ie6.css' ?>" />
    <![endif]-->
  </head>
  <body class = "<?php print $body_classes ?>">

  <!-- Page Layout -->
  <div id="page-wrapper" class="clearfix">
    <div id="content-wrapper" class="clearfix">
      <div id="header-upper">
        <?php print $header_upper ?>
      </div><!-- end header-upper -->

      <div id="left-column">
        <div id="header-wrapper" class="clearfix">
          <div id="header" class="clearfix">
            <?php print ($logo) ? '<div id="site-logo"><img src = "' . $logo . '" alt = "' . $site_name . '" /></div>' : '' ?>
            <?php print ($site_name) ? '<div id="site-name">' . l($site_name, '<front>') . '</div>' : '' ?>
            <?php print $header ?>
          </div><!-- end header -->
        </div><!-- end header-wrapper -->
        <div id="content-column" class="clearfix">
          <?php print $breadcrumb ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clearfix">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print $tabs .'</div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <div id="page-content">
            <?php print $content ?>
          </div>
          <?php print $feed_icons ?>
        </div><!-- end content-column -->
      </div><!-- end left-column -->
    
      <div id="right-wrapper" class="clearfix">
        <div id="head-block-wrapper" class="clearfix">
          <div id="head-block-top-left"></div>
          <div id="head-block-top-center"></div>
          <div id="head-block-top-right"></div>
          <div id="head-block-content-outer">
            <div id="head-block-content-inner">
            <div id="head-block-content" class="clearfix">
              <?php print ($mission)? '<div id="mission">'. $mission .'</div>' : '&nbsp;' ?>
              <?php if (isset($primary_links)) : ?>
                <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
              <?php endif; ?>
              <?php if (isset($secondary_links)) : ?>
                <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
              <?php endif; ?>
              <?php if ($search_box): print $search_box; endif; ?>
              <?php print $right_upper ?>
            </div>
          </div>
          </div>
          <div id="head-block-bottom-left"></div>
          <div id="head-block-bottom-center"></div>
          <div id="head-block-bottom-right"></div>

        </div><!-- end head-block-wrapper -->

        <?php if ($right): ?>
          <div id="right-sidebar" class="clearfix">
            <?php print $right ?>
          </div><!-- end right-sidebar -->
        <?php endif ?>
      </div><!-- end right-wrapper -->

      <div id="footer-wrapper" class="clearfix">
        <div id="footer" class="clearfix">
          <?php print ($footer_message) ? $footer_message : "" ?>
          <?php print $footer ?>
        </div><!-- end footer -->
        <div id="footer-lower" class="clearfix">
          <?php print $footer_lower ?>
        </div><!-- end footer-lower -->
      </div><!-- end footer-wrapper -->
    </div><!-- end content-wrapper -->
  </div><!-- end page-wrapper -->
  <!-- /Page Layout -->

  <?php print $closure ?>
  </body>
</html>
